package com.example.splash_screen_animation;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView textView;
    private TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(imageView, "imageapp");
                pairs[1] = new Pair<View, String>(textView, "textapp");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent, options.toBundle());
            }
        }, 3000);

        imageView.setTranslationY(-1000f);
        imageView.setAlpha(0f);
        imageView.animate().translationY(0f).rotation(360f).alpha(1f).setDuration(2000);

        textView.setTranslationY(1000f);
        textView.setRotation(-180);
        textView.animate().translationY(0f).rotation(0).setDuration(1500).setStartDelay(500);

        textView2.setTranslationY(1000f);
        textView2.setRotation(180);
        textView2.animate().translationY(0f).rotation(0).setDuration(1000).setStartDelay(1000);

    }
}