package com.example.splash_screen_animation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {
    private RelativeLayout relativeLayoutDashboard;
    private TextView newUserText;
    private TextView forgotText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        relativeLayoutDashboard = findViewById(R.id.relativeLayoutDashboard);
        newUserText = findViewById(R.id.newUserText);
        forgotText = findViewById(R.id.forgotText);

        relativeLayoutDashboard.setAlpha(0f);
        relativeLayoutDashboard.setScaleX(0);
        relativeLayoutDashboard.setScaleY(0);
        relativeLayoutDashboard.animate().alpha(1f).scaleX(1).scaleY(1).setDuration(500);

        newUserText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimacioText(newUserText);

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashboardActivity.this, SignupActivity.class);
                        startActivity(intent);
                    }
                },250);
            }
        });

        forgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimacioText(forgotText);

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashboardActivity.this, ForgotActivity.class);
                        startActivity(intent);
                    }
                },250);
            }
        });


    }

    public static void AnimacioText (TextView textView) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(textView, "scaleX", 0.85f, 1);
        objectAnimator.setDuration(250);

        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(textView, "scaleY", 0.85f, 1);
        objectAnimator.setDuration(250);

        AnimatorSet animatorSetAug = new AnimatorSet();
        animatorSetAug.playTogether(objectAnimator, objectAnimator2);
        animatorSetAug.start();
    }
}