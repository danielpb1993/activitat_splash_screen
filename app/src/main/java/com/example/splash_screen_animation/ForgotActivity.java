package com.example.splash_screen_animation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ForgotActivity extends AppCompatActivity {
    private TextView newUserForgot;
    private RelativeLayout relativeLayoutForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        newUserForgot = findViewById(R.id.newUserForgot);
        relativeLayoutForgot = findViewById(R.id.relativeLayoutforgot);

        relativeLayoutForgot.setAlpha(0f);
        relativeLayoutForgot.setScaleX(0);
        relativeLayoutForgot.setScaleY(0);
        relativeLayoutForgot.animate().alpha(1f).scaleX(1).scaleY(1).setDuration(500);

        newUserForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashboardActivity.AnimacioText(newUserForgot);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ForgotActivity.this, SignupActivity.class);
                        startActivity(intent);
                    }
                },500);
            }
        });
    }
}