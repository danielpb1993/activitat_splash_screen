package com.example.splash_screen_animation;

public class Bicicleta {
    private String tipusBici;
    private int imatgeBici;

    public Bicicleta(String tipusBici, int imatgeBici) {
        this.tipusBici = tipusBici;
        this.imatgeBici = imatgeBici;
    }

    public String getTipusBici() {
        return tipusBici;
    }

    public void setTipusBici(String tipusBici) {
        this.tipusBici = tipusBici;
    }

    public int getImatgeBici() {
        return imatgeBici;
    }

    public void setImatgeBici(int imatgeBici) {
        this.imatgeBici = imatgeBici;
    }
}
