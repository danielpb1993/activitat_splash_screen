package com.example.splash_screen_animation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class BicicletaAdapter extends ArrayAdapter<Bicicleta> {
    public BicicletaAdapter(Context context, ArrayList<Bicicleta> bicicletaArrayList) {
        super(context, 0, bicicletaArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);

    }

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row,
                    parent,
                    false
            );
            ImageView imageView = convertView.findViewById(R.id.bici1);
            TextView textView = convertView.findViewById(R.id.textBici);

            Bicicleta currentItem = getItem(position);
            if (currentItem != null) {
                imageView.setImageResource(currentItem.getImatgeBici());
                textView.setText(currentItem.getTipusBici());
            }
        }
        return convertView;
    }
}
