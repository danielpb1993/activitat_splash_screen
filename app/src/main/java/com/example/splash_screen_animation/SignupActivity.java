package com.example.splash_screen_animation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;

public class SignupActivity extends AppCompatActivity {

    private DatePickerDialog picker;
    private TextInputEditText textDate1;
    private Spinner spinner;

    private ArrayList<Bicicleta> mBicicleta;
    private BicicletaAdapter mAdapter;

    private SwitchCompat switchEnableDisable;
    private TextView alreadyHaveText;

    private RelativeLayout relativeLayoutSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        textDate1 = findViewById(R.id.textDate1);
        spinner = findViewById(R.id.spinner);
        switchEnableDisable = findViewById(R.id.switchNewsletter);
        alreadyHaveText = findViewById(R.id.alreadyHaveText);
        relativeLayoutSignup = findViewById(R.id.relativeLayoutSignup);

        relativeLayoutSignup.setAlpha(0f);
        relativeLayoutSignup.setScaleX(0);
        relativeLayoutSignup.setScaleY(0);
        relativeLayoutSignup.animate().alpha(1f).scaleX(1).scaleY(1).setDuration(500);

        textDate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                picker = new DatePickerDialog(SignupActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                textDate1.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancel·la", picker);
                picker.setButton(DatePickerDialog.BUTTON_POSITIVE, "D'acord", picker);
                picker.show();
            }
        });

        mBicicleta = new ArrayList<>();
        mBicicleta.add(new Bicicleta("Mountain Bike", R.drawable.bike01));
        mBicicleta.add(new Bicicleta("Cycling Bike", R.drawable.bike02));
        mBicicleta.add(new Bicicleta("Road Bike", R.drawable.bike08));
        mBicicleta.add(new Bicicleta("Folding Bike", R.drawable.bike03));
        mBicicleta.add(new Bicicleta("Hybrid Bike", R.drawable.bike04));
        mBicicleta.add(new Bicicleta("Touring Bike", R.drawable.bike06));

        mAdapter = new BicicletaAdapter(this, mBicicleta);
        spinner.setAdapter(mAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Bicicleta selectedItem = (Bicicleta) parent.getItemAtPosition(position);
                String selectedBicicleta = selectedItem.getTipusBici();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(SignupActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        if (switchEnableDisable != null)
            switchEnableDisable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                }
            });

        alreadyHaveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashboardActivity.AnimacioText(alreadyHaveText);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SignupActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }
                },250);
            }
        });
    }
}